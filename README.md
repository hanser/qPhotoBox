# qPhotoBox

PhotoBooth Project

## Features

* Good image quality
    * because of a big image sensor (DSLR)
    * because of a good flash setup
* Remote control
    * Presenter-style remote control to trigger the recording (can easily be hidden e.g. behind the back)
    * Back and forth buttons on the remote control to view the last 5 images
    * No delayed-action shutter release you press at the box and rush back to your spot
    * No "buzzer"-Button (or the gesture to press it) visible on every picture
* Live view
    * The image preview is displayed all the time so you can position in front of the camera properly
* Big screen
    * No tiny 7 or 9'' Raspberry Pi touch display
* Relatively low entrance costs for what you get
    * Upgradable/Replacable components (start small and go big)
* No Printer
    * This is the main reason for high costs at an event
    * A lot of waste is produced
        * Many images are too bad to be printed
        * The guests need to take care for the prints and to not forget them when they leave
    * It is (in my opinion) way better to print the _nice_ pictures cheaper and in bulk afterwards and send them to the guests with e.g. a thank-you card.

## Compile and installation instructions
TODO

## Build a PhotoBox
Details about how to build one yourself can be found in the [hardware-wiki page](https://codeberg.org/hanser/qPhotoBox/wiki/Hardware)

