#include "KeyPressFilter.h"

#include <QDebug>
#include <QEvent>
#include <QKeyEvent>

KeyPressFilter::KeyPressFilter(QObject *parent) : QObject(parent)
{
    configurationTimer.setSingleShot(true);
}

bool KeyPressFilter::eventFilter(QObject *obj, QEvent *event)
{
    bool enableConfig = false;
    bool enableFocus = false;
    switch (event->type())
    {
        case QEvent::KeyPress:
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            switch(keyEvent->key())
            {
                case 16777238: // remote up
                case 16777235: // arrow up                
                    if(enableConfig && configurationTimer.isActive())
                    {
                        qDebug("Config-UP");
                        configurationTimer.start(3000);
                        emit increaseBrightness();
                    }
                    else if(enableFocus && focusTimer.isActive())
                    {
                        qDebug("Focus-UP");
                        focusTimer.start(3000);
                        emit executeFocus();
                    }
                    else
                    {
                        qDebug("UP");
                        emit upPressed();
                    }
                    break;
                case 16777239: // remote down
                case 16777237: // arrow down
                    if(enableConfig && configurationTimer.isActive())
                    {
                        qDebug("Config-DOWN");
                        configurationTimer.start(3000);
                        emit decreaseBrightness();
                    }
                    else
                    {
                        qDebug("DOWN");
                        emit downPressed();
                    }
                    break;
                case 16777217: // remote rect
                case 32: // SPACE
                    configurationTimer.stop();
                    focusTimer.stop();
                    emit takePicturePressed();
                    qDebug("TAKE PICTURE");
                    break;
                case 16777248: // + hold key up
                case 16777268: // | hold key up
                case 16777216: // + hold key up
                    qDebug() << "RESET" << "Key pressed:" << keyEvent->key();
                    break;
                case 66:       // hold key down
                    if(enableConfig)
                    {
                        qDebug() << "Configuration timer start";
                        configurationTimer.start(3000);
                    }
                    break;
                case 16777251: // hold "take picture"
                    if(enableFocus)
                    {
                        qDebug() << "Focus timer start";
                        focusTimer.start(3000);
                    }
                    break;
                case 81: // q
                    emit increaseShutterSpeed();
                    break;
                case 65: // a
                    emit decreaseShutterSpeed();
                    break;
                case 87: // w
                    emit increaseAperture();
                    break;
                case 83: // s
                    emit decreaseAperture();
                    break;
                case 69: // e
                    emit increaseISO();
                    break;
                case 68: // d
                    emit decreaseISO();
                    break;
                case 70: // f
                    emit toggleFocusMode();
                    break;
                case 82: // r
                    emit executeFocus();
                    break;
                default:
                    qDebug("Key pressed: %d", keyEvent->key());
                    break;
            }
            return true;
            break;
        }
        default:
            // standard event processing
            return QObject::eventFilter(obj, event);
            break;
    }
}
