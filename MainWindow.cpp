#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QPhotoLib.h>
#include <QDebug>
#include <QCameraTreeModel.h>
#include <QMetaType>

#include <KeyPressFilter.h>
#include <ImageConverter.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint);
    this->showFullScreen();

    recordingDir = "Aufnahmen";

    historyPosition = -1;
    historyTimer = new QTimer(this);
    connect(historyTimer, &QTimer::timeout, this, &MainWindow::stopHistory);

    qPhotoLib = new QPhotoLib();

    connect(qPhotoLib,  &QPhotoLib::apertureValues,         this, &MainWindow::slotApertureValues,      Qt::QueuedConnection);
    connect(qPhotoLib,  &QPhotoLib::shutterSpeedValues,     this, &MainWindow::slotShutterSpeedValues,  Qt::QueuedConnection);
    connect(qPhotoLib,  &QPhotoLib::iSOValues,              this, &MainWindow::slotISOValues,           Qt::QueuedConnection);
    connect(qPhotoLib,  &QPhotoLib::focusModeValues,        this, &MainWindow::slotFocusModeValues,     Qt::QueuedConnection);

    connect(qPhotoLib,  &QPhotoLib::cameraConnected,        this, &MainWindow::cameraConnected,         Qt::QueuedConnection);
    connect(qPhotoLib,  &QPhotoLib::pictureTaken,           this, &MainWindow::pictureTaken,            Qt::QueuedConnection);
    connect(qPhotoLib,  &QPhotoLib::signalExecutedFocus,    this, &MainWindow::startPreview,            Qt::QueuedConnection);

    keyPressFilter = new KeyPressFilter(this);
    this->installEventFilter(keyPressFilter);

    connect(keyPressFilter, &KeyPressFilter::takePicturePressed,   this, &MainWindow::displayEyesUpImage,   Qt::QueuedConnection);
    //connect(keyPressFilter, &KeyPressFilter::takePicturePressed,   this, &MainWindow::takePicture,          Qt::QueuedConnection);

    connect(keyPressFilter, &KeyPressFilter::upPressed,            this, &MainWindow::goForth,              Qt::QueuedConnection);
    connect(keyPressFilter, &KeyPressFilter::downPressed,          this, &MainWindow::goBack,               Qt::QueuedConnection);

    connect(keyPressFilter, &KeyPressFilter::increaseShutterSpeed, this, &MainWindow::increaseShutterSpeed, Qt::QueuedConnection);
    connect(keyPressFilter, &KeyPressFilter::decreaseShutterSpeed, this, &MainWindow::decreaseShutterSpeed, Qt::QueuedConnection);

    connect(keyPressFilter, &KeyPressFilter::increaseAperture,     this, &MainWindow::increaseAperture,     Qt::QueuedConnection);
    connect(keyPressFilter, &KeyPressFilter::decreaseAperture,     this, &MainWindow::decreaseAperture,     Qt::QueuedConnection);

    connect(keyPressFilter, &KeyPressFilter::increaseISO,          this, &MainWindow::increaseISO,          Qt::QueuedConnection);
    connect(keyPressFilter, &KeyPressFilter::decreaseISO,          this, &MainWindow::decreaseISO,          Qt::QueuedConnection);

    connect(keyPressFilter, &KeyPressFilter::increaseBrightness,   this, &MainWindow::increaseBrightness,   Qt::QueuedConnection);
    connect(keyPressFilter, &KeyPressFilter::decreaseBrightness,   this, &MainWindow::decreaseBrightness,   Qt::QueuedConnection);

    connect(keyPressFilter, &KeyPressFilter::toggleFocusMode,      this, &MainWindow::toggleFocusMode,      Qt::QueuedConnection);
    connect(keyPressFilter, &KeyPressFilter::executeFocus,         this, &MainWindow::executeFocus,         Qt::QueuedConnection);

    connect(&configTimer,   &QTimer::timeout,                      this, &MainWindow::hideConfiguration,    Qt::QueuedConnection);
    hideConfiguration();
}

MainWindow::~MainWindow()
{
    delete keyPressFilter;
    delete qPhotoLib;
    delete ui;
}

void MainWindow::cameraConnected()
{
    qDebug() << "MW::cameraConnected";
    //qPhotoLib->takePicture(currentPicture);
    startPreview();
}

void MainWindow::pictureTaken(QImage currentPicture)
{
    qDebug() << "MW::pictureTaken";
    QPixmap currentImagePixmap = QPixmap::fromImage(currentPicture);
    QPixmap pixmapToDraw = currentImagePixmap.scaled(ui->currentImage->width(),ui->currentImage->height(),Qt::KeepAspectRatio);
    ui->currentImage->setPixmap(pixmapToDraw);

    //save image
    if(!QDir(recordingDir).exists())
    {
        QDir().mkdir(recordingDir);
    }
    currentPicture.save(QString("%1/%2.jpg").arg(recordingDir).arg(QDateTime::currentDateTime().toString("yyyy-MM-ddTHH-mm-ss")),"JPEG",95);

    //qPhotoLib->takePicture(currentPicture);

    connect(keyPressFilter, &KeyPressFilter::takePicturePressed,   this, &MainWindow::displayEyesUpImage,   Qt::QueuedConnection);
    //connect(keyPressFilter, &KeyPressFilter::takePicturePressed,   this, &MainWindow::takePicture,          Qt::QueuedConnection);

    QTimer::singleShot(1000, this, &MainWindow::startPreview);
}

void MainWindow::previewPictureTaken(QImage currentPicture)
{
    //qDebug() << "MW::previewPictureTaken" << timeLastPictureTaken.msecsTo(QTime::currentTime());
    timeLastPictureTaken = QTime::currentTime();

    QPixmap currentImagePixmap;
    int i = 2;
    if(i == 1)
    {
        cv::Mat rgb_image = ImageConverter::QImageToCvMat(currentPicture);
        cv::Mat lab_image;
        cv::cvtColor(rgb_image, lab_image, CV_RGB2Lab);

        // Extract the L channel
        std::vector<cv::Mat> lab_planes(3);
        cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]

        // apply the CLAHE algorithm to the L channel
        cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
        clahe->setClipLimit(4);
        cv::Mat dst;
        clahe->apply(lab_planes[0], dst);

        // Merge the the color planes back into an Lab image
        dst.copyTo(lab_planes[0]);
        cv::merge(lab_planes, lab_image);

        // convert back to RGB
        cv::Mat image_clahe;
        cv::cvtColor(lab_image, image_clahe, CV_Lab2RGB);

        currentImagePixmap = ImageConverter::cvMatToQPixmap(image_clahe);
    }
    else if(i==2)
    {
        QImage localImage = QImage(currentPicture);
        cv::Mat rgb_image = ImageConverter::QImageToCvMat(localImage, false);
        whiteBalance(rgb_image);
//        cv::Mat lab_image;
//        cv::cvtColor(rgb_image, lab_image, CV_RGB2Lab);
//        cv::Mat image_clahe;
//        cv::cvtColor(lab_image, image_clahe, CV_Lab2RGB);
        currentImagePixmap = ImageConverter::cvMatToQPixmap(rgb_image);
    }
    else
    {
        currentImagePixmap = QPixmap::fromImage(currentPicture);
    }
    QPixmap pixmapToDraw = currentImagePixmap.scaled(ui->currentImage->width(),ui->currentImage->height(),Qt::KeepAspectRatio);
    ui->currentImage->setPixmap(pixmapToDraw.transformed(QTransform().scale(-1, 1)));
}

void MainWindow::takePicture()
{
    stopPreview();
    disconnect(keyPressFilter, &KeyPressFilter::takePicturePressed,   this, &MainWindow::displayEyesUpImage);
    //disconnect(keyPressFilter, &KeyPressFilter::takePicturePressed,   this, &MainWindow::takePicture);
    qPhotoLib->takePicture();
}

void MainWindow::startPreview()
{
    connect(qPhotoLib,  &QPhotoLib::previewPictureTaken,    this, &MainWindow::previewPictureTaken,     Qt::QueuedConnection);

    qPhotoLib->startPreview(50);
}

void MainWindow::stopPreview()
{
    disconnect(qPhotoLib,  &QPhotoLib::previewPictureTaken,    this, &MainWindow::previewPictureTaken);

    qPhotoLib->stopPreview();
}

void MainWindow::goBack()
{
    if(historyPosition < 4)
    {
        historyPosition++;
        displayHistoricImage();
    }
    historyTimer->start(10000);
}

void MainWindow::goForth()
{
    if(historyPosition > -1)
    {
        historyPosition--;
        displayHistoricImage();
    }
    historyTimer->start(10000);
}

void MainWindow::stopHistory()
{
    historyTimer->stop();
    historyPosition = -1;
    startPreview();
}

void MainWindow::displayHistoricImage()
{
    stopPreview();
    if(!QDir(recordingDir).exists())
    {
        QDir().mkdir(recordingDir);
    }

    QStringList images = QDir(recordingDir).entryList(QStringList() << "*.jpg",QDir::Files, QDir::Name | QDir::Reversed);

    historyPosition = qMin(historyPosition, images.size()-1);

    if(historyPosition == -1)
    {
        stopHistory();
    }
    else
    {
        qDebug() << "loading" << historyPosition << QString("%1/%2").arg(recordingDir).arg(images.at(historyPosition));

        QPixmap currentImagePixmap = QPixmap::fromImage(QImage(QString("%1/%2").arg(recordingDir).arg(images.at(historyPosition))));
        QPixmap pixmapToDraw = currentImagePixmap.scaled(ui->currentImage->width(),ui->currentImage->height(),Qt::KeepAspectRatio);
        ui->currentImage->setPixmap(pixmapToDraw);
    }
}

void MainWindow::displayEyesUpImage()
{
    stopPreview();

    historyTimer->stop();
    historyPosition = -1;

    QPixmap currentImagePixmap = QPixmap::fromImage(QImage(":/eyes_up"));
    QPixmap pixmapToDraw = currentImagePixmap.scaled(ui->currentImage->width(),ui->currentImage->height(),Qt::KeepAspectRatio);
    ui->currentImage->setPixmap(pixmapToDraw);

    QTimer::singleShot(700, this, &MainWindow::takePicture);
}

void MainWindow::whiteBalance(cv::Mat mat)
{
    //qDebug() << "channels" << mat.channels();
    double discard_ratio = 0.05;
    int hists[3][256];
    memset(hists, 0, 3*256*sizeof(int));

    for (int y = 0; y < mat.rows; ++y)
    {
        uchar* ptr = mat.ptr<uchar>(y);
        for (int x = 0; x < mat.cols; ++x)
        {
            for (int j = 0; j < 3; ++j)
            {
                hists[j][ptr[x * 4 + j]] += 1;
            }
        }
    }

    // cumulative hist
    int total = mat.cols*mat.rows;
    int vmin[3], vmax[3];
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 255; ++j)
        {
            hists[i][j + 1] += hists[i][j];
        }
        vmin[i] = 0;
        vmax[i] = 255;
        while (hists[i][vmin[i]] < discard_ratio * total)
        {
            vmin[i] += 1;
        }
        while (hists[i][vmax[i]] > (1 - discard_ratio) * total)
        {
            vmax[i] -= 1;
        }
        if (vmax[i] < 255 - 1)
        {
            vmax[i] += 1;
        }
    }
    //qDebug() << vmin[0] << vmax[0] << vmin[1] << vmax[1] << vmin[2] << vmax[2];
    int absMin = 255;
    int absMax = 0;
    for (int i = 0; i < 3; ++i)
    {
        if(vmin[i] < absMin)
        {
            absMin = vmin[i];
        }
        if(vmax[i] > absMax)
        {
            absMax = vmax[i];
        }
    }

    if(absMax-absMin < 10)
    {
        return;
    }

    for (int y = 0; y < mat.rows; ++y)
    {
        uchar* ptr = mat.ptr<uchar>(y);
        for (int x = 0; x < mat.cols; ++x)
        {
            for (int j = 0; j < 3; ++j)
            {
                int val = ptr[x * 4 + j];
                if (val < vmin[j])
                {
                    val = vmin[j];
                }
                if (val > vmax[j])
                {
                    val = vmax[j];
                }
                ptr[x * 4 + j] = static_cast<uchar>((val - absMin) * 255.0 / (absMax - absMin));
            }
        }
    }
}

void MainWindow::slotApertureValues(QStringList optionList,int currentIndex)
{
    qDebug() << optionList;
    apertureValues = optionList;
    currentApertureIndex = currentIndex;
    double darkestApertureValue = 0;
    double brightestApertureValue = 9999999999;
    for(int index=0; index < apertureValues.size(); index++)
    {
        double value = apertureValues.value(index, "0").replace("F ", "").replace(",", ".").toDouble();
        if(value < brightestApertureValue && value >= 5)
        {
            brightestApertureValue = value;
            brightestApertureIndex = index;
        }
        if(value > darkestApertureValue)
        {
            darkestApertureValue = value;
            darkestApertureIndex = index;
        }
    }
    qDebug() << "darkestAperture" << darkestApertureIndex << apertureValues.value(darkestApertureIndex, "0");
    qDebug() << "brightestAperture" << brightestApertureIndex << apertureValues.value(brightestApertureIndex, "0");
}

void MainWindow::slotShutterSpeedValues(QStringList optionList,int currentIndex)
{
    shutterSpeedValues = optionList;
    currentShutterSpeedIndex = currentIndex;
    double darkestShutterSpeedValue = 9999999999;
    double brightestShutterSpeedValue = 0;
    for(int index=0; index < shutterSpeedValues.size(); index++)
    {
        double value = shutterSpeedValues.value(index, "0").replace("s", "").replace(",", ".").toDouble();
        if(value < darkestShutterSpeedValue && value >= 0.006)
        {
            darkestShutterSpeedValue = value;
            darkestShutterSpeedIndex = index;
        }
        if(value > brightestShutterSpeedValue && value <= 0.02)
        {
            brightestShutterSpeedValue = value;
            brightestShutterSpeedIndex = index;
        }
    }
    qDebug() << "darkestShutterSpeed" << darkestShutterSpeedIndex << shutterSpeedValues.value(darkestShutterSpeedIndex, "0");
    qDebug() << "brightestShutterSpeed" << brightestShutterSpeedIndex << shutterSpeedValues.value(brightestShutterSpeedIndex, "0");
}

void MainWindow::slotISOValues(QStringList optionList,int currentIndex)
{
    isoValues = optionList;
    currentIsoIndex = currentIndex;
    double darkestISOValue = 9999999999;
    double brightestISOValue = 0;
    for(int index=0; index < isoValues.size(); index++)
    {
        double value = isoValues.value(index, "0").replace("ISO", "").replace(",", ".").toDouble();
        if(value < darkestISOValue)
        {
            darkestISOValue = value;
            darkestISOIndex = index;
        }
        if(value > brightestISOValue && value <= 6400)
        {
            brightestISOValue = value;
            brightestISOIndex = index;
        }
    }
    qDebug() << "darkestISO" << darkestISOIndex << isoValues.value(darkestISOIndex, "0");
    qDebug() << "brightestISO" << brightestISOIndex << isoValues.value(brightestISOIndex, "0");
}

void MainWindow::slotFocusModeValues(QStringList optionList,int currentIndex)
{
    focusModeValues = optionList;
    currentFocusModeIndex = currentIndex;
}

void MainWindow::increaseShutterSpeed()
{
    if(currentShutterSpeedIndex < shutterSpeedValues.size()-1)
    {
        currentShutterSpeedIndex++;
        sendShutterSpeed();
    }
}
void MainWindow::decreaseShutterSpeed()
{
    if(currentShutterSpeedIndex > 0)
    {
        currentShutterSpeedIndex--;
        sendShutterSpeed();
    }
}

void MainWindow::increaseAperture()
{
    if(currentApertureIndex < apertureValues.size()-1)
    {
        currentApertureIndex++;
        sendAperture();
    }
}
void MainWindow::decreaseAperture()
{
    if(currentApertureIndex > 0)
    {
        currentApertureIndex--;
        sendAperture();
    }
}

void MainWindow::increaseISO()
{
    if(currentIsoIndex < isoValues.size()-1)
    {
        currentIsoIndex++;
        sendISO();
    }
}
void MainWindow::decreaseISO()
{
    if(currentIsoIndex > 0)
    {
        currentIsoIndex--;
        sendISO();
    }
}

void MainWindow::increaseBrightness()
{
    qDebug() << "MW::increaseBrightness";
    if(currentApertureIndex > brightestApertureIndex)
    {
        decreaseAperture();
        return;
    }
    if(currentShutterSpeedIndex < brightestShutterSpeedIndex)
    {
        increaseShutterSpeed();
        return;
    }
    if(currentIsoIndex < brightestISOIndex)
    {
        increaseISO();
        return;
    }
}

void MainWindow::decreaseBrightness()
{
    qDebug() << "MW::decreaseBrightness";
    if(currentIsoIndex > darkestISOIndex)
    {
        decreaseISO();
        return;
    }
    if(currentShutterSpeedIndex > darkestShutterSpeedIndex)
    {
        decreaseShutterSpeed();
        return;
    }
    if(currentApertureIndex < darkestApertureIndex)
    {
        increaseAperture();
        return;
    }}

void MainWindow::toggleFocusMode()
{
    if(currentFocusModeIndex < focusModeValues.size()-1)
    {
        currentFocusModeIndex++;
    }
    else
    {
        currentFocusModeIndex=0;
    }
    sendFocusMode();
}

void MainWindow::executeFocus()
{
    qPhotoLib->executeFocus();
}

void MainWindow::sendShutterSpeed()
{
    qPhotoLib->setShutterSpeed(shutterSpeedValues.value(currentShutterSpeedIndex));
    showConfiguration();
}

void MainWindow::sendAperture()
{
    qPhotoLib->setAperture(apertureValues.value(currentApertureIndex));
    showConfiguration();
}

void MainWindow::sendISO()
{
    qPhotoLib->setISO(isoValues.value(currentIsoIndex));
    showConfiguration();
}

void MainWindow::sendFocusMode()
{
    qPhotoLib->setFocusMode(focusModeValues.value(currentFocusModeIndex));
    showConfiguration();
}

void MainWindow::showConfiguration()
{
    configTimer.stop();
    ui->centerStackedWidget->setCurrentWidget(ui->configPage);
    ui->configLabel->setText(QString("%1 %2 %3 %4").arg(shutterSpeedValues.value(currentShutterSpeedIndex)).arg(apertureValues.value(currentApertureIndex)).arg(isoValues.value(currentIsoIndex)).arg(focusModeValues.value(currentFocusModeIndex)));
    configTimer.start(1000);
}

void MainWindow::hideConfiguration()
{
    ui->centerStackedWidget->setCurrentWidget(ui->cameraPage);
}
