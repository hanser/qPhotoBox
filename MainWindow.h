#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>
#include <QTimer>

#include <opencv2/core/core.hpp>

class QPhotoLib;
class KeyPressFilter;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void cameraConnected();
    void pictureTaken(QImage currentPicture);
    void previewPictureTaken(QImage currentPicture);
    void takePicture();
    void displayEyesUpImage();

    void startPreview();
    void stopPreview();

    void goBack();
    void goForth();
    void stopHistory();
    void slotApertureValues(QStringList optionList, int currentIndex);
    void slotShutterSpeedValues(QStringList optionList, int currentIndex);
    void slotISOValues(QStringList optionList, int currentIndex);
    void slotFocusModeValues(QStringList optionList, int currentIndex);

    void increaseShutterSpeed();
    void decreaseShutterSpeed();
    void increaseAperture();
    void decreaseAperture();
    void increaseISO();
    void decreaseISO();
    void toggleFocusMode();
    void executeFocus();

    void increaseBrightness();
    void decreaseBrightness();

    void hideConfiguration();
    void showConfiguration();

private:
    Ui::MainWindow *ui;
    QPhotoLib* qPhotoLib;
    QTime timeLastPictureTaken;
    KeyPressFilter* keyPressFilter;

    int historyPosition;
    QString recordingDir;
    QTimer* historyTimer;

    void displayHistoricImage();
    void whiteBalance(cv::Mat mat);

    QStringList apertureValues;
    int currentApertureIndex;
    int darkestApertureIndex;
    int brightestApertureIndex;

    QStringList shutterSpeedValues;
    int currentShutterSpeedIndex;
    int darkestShutterSpeedIndex;
    int brightestShutterSpeedIndex;

    QStringList isoValues;
    int currentIsoIndex;
    int darkestISOIndex;
    int brightestISOIndex;

    QStringList focusModeValues;
    int currentFocusModeIndex;

    QTimer configTimer;

    void sendShutterSpeed();
    void sendAperture();
    void sendISO();
    void sendFocusMode();

};

#endif // MAINWINDOW_H
