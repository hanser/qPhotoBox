QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qPhotoBox
TEMPLATE = app


SOURCES += main.cpp\
    MainWindow.cpp \
    KeyPressFilter.cpp

HEADERS  += MainWindow.h \
    KeyPressFilter.h

FORMS    += MainWindow.ui

linux:!mac {
    # for debian
    INCLUDEPATH += /usr/include/opencv
    LIBS += -L/usr/lib/x86_64-linux-gnu/
    # for alpine
    INCLUDEPATH += /usr/local/include/opencv4
    LIBS += -L/usr/local/lib64/
    LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc
    LIBS += -lgphoto2
}

INCLUDEPATH += $$quote($$_PRO_FILE_PWD_/libs/qPhotoLib)
CONFIG(debug, debug|release) {
    LIBS += -L$$quote($$OUT_PWD/libs/qPhotoLib) -lqPhotoLibd
} else {
    LIBS += -L$$quote($$OUT_PWD/libs/qPhotoLib) -lqPhotoLib
}

INCLUDEPATH += $$PWD/libs/qPhotoLib
DEPENDPATH += $$PWD/libs/qPhotoLib

RESOURCES += \
    resources.qrc
