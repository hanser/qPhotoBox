#ifndef KEYPRESSFILTER_H
#define KEYPRESSFILTER_H

#include <QObject>
#include <QTimer>

class KeyPressFilter : public QObject
{
    Q_OBJECT
public:
    explicit KeyPressFilter(QObject *parent = nullptr);

signals:
    void upPressed();
    void downPressed();
    void takePicturePressed();

    void increaseShutterSpeed();
    void decreaseShutterSpeed();
    void increaseAperture();
    void decreaseAperture();
    void increaseISO();
    void decreaseISO();
    void toggleFocusMode();
    void executeFocus();

    void increaseBrightness();
    void decreaseBrightness();

protected:
    bool eventFilter(QObject *obj, QEvent *event);

    QTimer configurationTimer;
    QTimer focusTimer;
};

#endif // KEYPRESSFILTER_H
